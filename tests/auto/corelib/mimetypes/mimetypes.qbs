import qbs

QtAutotestProject {
    name: "corelib_mimetype_tests"
    references: [
        "qmimedatabase",
        "qmimetype",
    ]
}
