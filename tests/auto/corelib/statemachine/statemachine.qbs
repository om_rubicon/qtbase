import qbs

QtAutotestProject {
    name: "corelib_statemachine_tests"
    references: [
        "qstate",
        "qstatemachine",
    ]
}
