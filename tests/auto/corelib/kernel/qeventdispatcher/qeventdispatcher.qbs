import qbs

QtAutotest {
    name: "tst_qeventdispatcher"
    supportsUiKit: true
    files: "tst_qeventdispatcher.cpp"
}
