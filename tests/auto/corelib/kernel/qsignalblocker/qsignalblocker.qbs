import qbs

QtAutotest {
    name: "tst_qsignalblocker"
    supportsUiKit: true
    files: "tst_qsignalblocker.cpp"
}
