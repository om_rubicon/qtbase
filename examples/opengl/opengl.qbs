import qbs

Project {
    name: "opengl"

    references: [
        "2dpainting",
        "contextinfo",
        "cube",
        "hellogl2",
        "hellogles3",
        "hellowindow",
        "paintedwindow",
        "qopenglwidget",
        "qopenglwindow",
        "textures",
        "threadedqopenglwidget",
    ]
}
