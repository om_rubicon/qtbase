import qbs

Project {
    name: "examples"
    references: [
        "examples-deployment.qbs",
        "examples-products.qbs",
    ]
}
