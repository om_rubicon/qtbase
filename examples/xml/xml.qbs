import qbs

Project {
    name: "xml"
    references: [
        "dombookmarks",
        "htmlinfo",
        "rsslisting",
        "saxbookmarks",
        "streambookmarks",
        "xmlstreamlint",
    ]
}
