import qbs

Project {
    name: "qtconcurrent"
    references: [
        "imagescaling",
        "map",
        "progressdialog",
        "runfunction",
        "wordcount",
    ]
}
