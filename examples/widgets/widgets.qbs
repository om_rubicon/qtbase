import qbs

Project {
    name: "widgets"

    references: [
        "animation",
        "desktop",
        "dialogs",
        "draganddrop",
        "effects",
        "gestures",
        "graphicsview",
        "itemviews",
        "layouts",
        "mac",
        "mainwindows",
        "painting",
        "richtext",
        "scroller",
        "statemachine",
        "tools",
        "tutorials",
        "widgets",
        "windowcontainer",
    ]
}
