import qbs

Project {
    name: "mainwindows"
    references: [
        "application",
        "dockwidgets",
        "mainwindow",
        "mdi",
        "menus",
        "sdi",
    ]
}
