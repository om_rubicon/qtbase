import qbs

Project {
    name: "dialogs"
    references: [
        "classwizard",
        "extension",
        "findfiles",
        "licensewizard",
        "standarddialogs",
        "tabdialog",
        "trivialwizard",
    ]
}
