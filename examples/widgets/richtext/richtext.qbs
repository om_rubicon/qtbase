import qbs

Project {
    name: "richtext"
    references: [
        "calendar",
        "orderform",
        "syntaxhighlighter",
        "textedit",
    ]
}
