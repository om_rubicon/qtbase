import qbs

Project {
    name: "draganddrop"
    references: [
        "draggableicons",
        "draggabletext",
        "dropsite",
        "fridgemagnets",
        "puzzle",
    ]
}
