import qbs

Project {
    name: "itemviews"
    references: [
        "addressbook",
        "basicsortfiltermodel",
        "chart",
        "coloreditorfactory",
        "combowidgetmapper",
        "customsortfiltermodel",
        "dirview",
        "editabletreemodel",
        "fetchmore",
        "frozencolumn",
        "interview",
        "pixelator",
        "puzzle",
        "simpledommodel",
        "simpletreemodel",
        "simplewidgetmapper",
        "spinboxdelegate",
        "spreadsheet",
        "stardelegate",
        "storageview",
    ]
}
