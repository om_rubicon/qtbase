import qbs

Project {
    name: "painting"
    references: [
        "affine",
        "basicdrawing",
        "composition",
        "concentriccircles",
        "deform",
        "fontsampler",
        "gradients",
        "imagecomposition",
        "painterpaths",
        "pathstroke",
        "transformations",
    ]
}
