import qbs

Project {
    name: "animation"
    references: [
        "animatedtiles",
        "easing",
        "moveblocks",
        "states",
        "stickman",
        "sub-attaq",
    ]
}
