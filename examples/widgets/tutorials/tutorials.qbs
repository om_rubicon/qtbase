import qbs

Project {
    name: "tutorials"
    references: [
        "addressbook",
        "gettingStarted",
        "modelview",
        "widgets",
    ]
}
