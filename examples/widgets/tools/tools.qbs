import qbs

Project {
    name: "tools"
    references: [
        "codecs",
        "completer",
        "customcompleter",
        "echoplugin",
        "i18n",
        "plugandpaint",
        "regexp",
        "regularexpression",
        "settingseditor",
        "styleplugin",
        "treemodelcompleter",
        "undo",
        "undoframework",
    ]
}
