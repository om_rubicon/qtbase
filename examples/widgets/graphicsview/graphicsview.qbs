import qbs

Project {
    name: "graphicsview"
    references: [
        "anchorlayout",
        "basicgraphicslayouts",
        "boxes",
        "chip",
        "collidingmice",
        "diagramscene",
        "dragdroprobot",
        "elasticnodes",
        "embeddeddialogs",
        "flowlayout",
        "padnavigator",
        "simpleanchorlayout",
        "weatheranchorlayout",
    ]
}
