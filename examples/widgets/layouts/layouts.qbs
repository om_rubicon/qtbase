import qbs

Project {
    name: "layouts"
    references: [
        "basiclayouts",
        "borderlayout",
        "dynamiclayouts",
        "flowlayout",
    ]
}
