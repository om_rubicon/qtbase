import qbs

Project {
    name: "corelib examples"
    references: [
        "ipc",
        "serialization",
        "threads",
        "tools",
    ]
}

