import qbs

Project {
    qbsSearchPaths: "."
    references: [
        "dummy-xml",
        "glsl",
        "lambda",
        "qparser",
    ]
}
