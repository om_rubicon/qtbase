import qbs
import QtGuiConfig

QtModuleProject {
    name: "QtAccessibilitySupport"
    simpleName: "accessibility_support"
    internal: true
    conditionFunction: (function() {
        return QtGuiConfig.accessibility;
    })

    QtHeaders {
    }

    QtModule {
        Export {
            Depends { name: "cpp" }
            Depends { name: "Qt.core" }
            cpp.includePaths: project.includePaths
        }

        Depends { name: project.headersName }
        Depends { name: "Qt.core_private" }
        Depends { name: "Qt.gui_private" }

        cpp.includePaths: project.includePaths.concat(base)
        cpp.defines: base.concat("QT_NO_CAST_FROM_ASCII")
        Group {
            files: [
                "qaccessiblebridgeutils.cpp",
                "qaccessiblebridgeutils_p.h",
            ]
        }
        Group {
            name: "Qt.core precompiled header"
            files: ["../../corelib/global/qt_pch.h"]
        }
    }
}
