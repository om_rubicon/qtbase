Group {
    prefix: path + "/"
    condition: product.config.graphicsview
    files: [
        "qgraph_p.h",
        "qgraphicsanchorlayout.cpp",
        "qgraphicsanchorlayout.h",
        "qgraphicsanchorlayout_p.cpp",
        "qgraphicsanchorlayout_p.h",
        "qgraphicsgridlayout.cpp",
        "qgraphicsgridlayout.h",
        "qgraphicsgridlayoutengine.cpp",
        "qgraphicsgridlayoutengine_p.h",
        "qgraphicsitem.cpp",
        "qgraphicsitem.h",
        "qgraphicsitem_p.h",
        "qgraphicsitemanimation.cpp",
        "qgraphicsitemanimation.h",
        "qgraphicslayout.cpp",
        "qgraphicslayout.h",
        "qgraphicslayout_p.cpp",
        "qgraphicslayout_p.h",
        "qgraphicslayoutitem.cpp",
        "qgraphicslayoutitem.h",
        "qgraphicslayoutitem_p.h",
        "qgraphicslayoutstyleinfo.cpp",
        "qgraphicslayoutstyleinfo_p.h",
        "qgraphicslinearlayout.cpp",
        "qgraphicslinearlayout.h",
        "qgraphicsproxywidget.cpp",
        "qgraphicsproxywidget.h",
        "qgraphicsproxywidget_p.h",
        "qgraphicsscene.cpp",
        "qgraphicsscene.h",
        "qgraphicsscene_bsp.cpp",
        "qgraphicsscene_bsp_p.h",
        "qgraphicsscene_p.h",
        "qgraphicsscenebsptreeindex.cpp",
        "qgraphicsscenebsptreeindex_p.h",
        "qgraphicssceneevent.cpp",
        "qgraphicssceneevent.h",
        "qgraphicssceneindex.cpp",
        "qgraphicssceneindex_p.h",
        "qgraphicsscenelinearindex.cpp",
        "qgraphicsscenelinearindex_p.h",
        "qgraphicstransform.cpp",
        "qgraphicstransform.h",
        "qgraphicstransform_p.h",
        "qgraphicsview.cpp",
        "qgraphicsview.h",
        "qgraphicsview_p.h",
        "qgraphicswidget.cpp",
        "qgraphicswidget.h",
        "qgraphicswidget_p.cpp",
        "qgraphicswidget_p.h",
        "qsimplex_p.cpp",
        "qsimplex_p.h",
    ]
}
