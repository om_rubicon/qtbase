Group {
    prefix: path + "/"
    files: [
        "qgraphicseffect.cpp",
        "qgraphicseffect.h",
        "qgraphicseffect_p.h",
        "qpixmapfilter.cpp",
        "qpixmapfilter_p.h",
    ]
}
