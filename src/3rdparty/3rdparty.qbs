import qbs

Project {
    name: "3rdparty"
    qbsSearchPaths: project.qtbaseShadowDir + "/src/plugins/sqldrivers/qbs"
    references: [
        "../angle/angle.qbs",
        "double-conversion.qbs",
        "pcre2.qbs",
        "zlib.qbs",
        // "dbus-linked.qbs",
        // "egl.qbs",
        // "fontconfig.qbs",
        "freetype/freetype.qbs",
        // "forkfd/forkfd.qbs",
        // "gbm.qbs",
        // "glesv2.qbs",
        // "glib.qbs",
        // "gl.qbs",
        // "gthread.qbs",
        // "gtk+.qbs",
        "harfbuzz.qbs",
        "iaccessible2",
        // "ice.qbs",
        // "libdrm.qbs",
        "libjpeg/libjpeg.qbs",
        "libpng/libpng.qbs",
        // "libinput.qbs",
        // "libudev.qbs",
        // "mtdev.qbs",
        // "pcre/pcre.qbs",
        // "sm.qbs",
        "sqlite",
        // "tslib.qbs",
        // "x11.qbs",
        // "x11-xcb.qbs",
        // "xcb/xcb.qbs",
        // "xcb/xcb-icccm.qbs",
        // "xcb/xcb-image.qbs",
        // "xcb/xcb-xkb.qbs",
        // "xcb/xcb-keysyms.qbs",
        // "xcb/xcb-randr.qbs",
        // "xcb/xcb-shape.qbs",
        // "xcb/xcb-sync.qbs",
        // "xcb/xcb-xfixes.qbs",
        // "xcb/xcb-xinerama.qbs",
        "xkbcommon.qbs",
    ]
}
